#include <iostream>
#include <vector>
#include <assert.h>
#include <openssl/bn.h>
#include <openssl/obj_mac.h>
#include <openssl/ecdsa.h>
#include <time.h>
#include <windows.h>

#include "libikv.h"
#include "scapdu.h"
#include "util.h"

/* Modify CKey::Sign:
 *  1. Comment the last three lines
 *  2. Add return ECDSA_Sign(vch, &hash, vchSig);
 *
bool CKey::Sign(const uint256 &hash, std::vector<unsigned char>& vchSig) const {
    if (!fValid)
        return false;
//    CECKey key;
//    key.SetSecretBytes(vch);
//    return key.Sign(hash, vchSig);
    return ECDSA_Sign(vch, &hash, vchSig);
}
*/

#define SCAPDU_VERBOSE_MSG

#define APDU_LEN         300 
#define APDURSP_LEN      300 
#define INPUT_MAX        1000 
  
/*** 
 * input: 
 *   -- sig_r: 32 bytes 
 *   -- sig_s: 32 bytes 
 *
 * output: 
 *   -- data: DER of (sig_r || sig_s) -> "index" bytes 
 */ 

static u4 sig_der(unsigned char* sig_r, unsigned char* sig_s, unsigned char *data) 
{
	/* Get ECDSA Signature */
	u1 tlen = 0x44;
	u1 rlen = 0x20;
	u1 slen = 0x20;
	u4 index = 0; 
	
	if (sig_r[0] & 0x80) {
		tlen++;
		rlen++;
	} 
	
	if (sig_s[0] & 0x80) {
		tlen++;
		slen++;
	}

	data[index++] = 0x30;
	data[index++] = tlen;
	data[index++] = 0x02;
	data[index++] = rlen;
	if (rlen == 0x21) {
		data[index++] = 0x00;
	}
	memcpy(data+index, sig_r, 32);
	index += 32;
	data[index++] = 0x02;
	data[index++] = slen;
	if (slen == 0x21) {
		data[index++] = 0x00;
	}
	memcpy(data+index, sig_s, 32);
	index += 32;

	return index;
}
  
bool SC_Sign(const unsigned char* vch, unsigned char* hash, std::vector<unsigned char>& vchSig) {
	int ret; 
	scapdu_ctx_t  scapdu_ctx; 
	BYTE  apdu[APDU_LEN]; 
	USIZ  apdu_len; 
	BYTE  apdursp[APDURSP_LEN]; 
	USIZ  apdursp_len; 
	char  inputs[INPUT_MAX]; 
  unsigned int nSize;
  FILE *fp;
  
  time_t rawtime;
  struct tm * timeinfo;
  
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  
  fp=fopen("libikv.log", "at");
  
  fprintf (fp, "SC_Sign %s", asctime (timeinfo) );
  
  fprintf(fp, "key: ");
  for(unsigned int i=0; i<32; i++)
      fprintf(fp, "%02X", vch[i]);
  fprintf(fp, "\n");
  
  fprintf(fp, "hash: ");
  for(unsigned int i=0; i<32; i++)
      fprintf(fp, "%02X", hash[i]);
  fprintf(fp, "\n");
  
  do {     	
  	/* Connect smartcard */ 
  	ret = scapdu_connect(&scapdu_ctx, NULL); 
  	if (ret != 0) {
      MessageBoxW (NULL, L"Please Insert BCDC Card", L"BCDC", MB_OK | MB_ICONINFORMATION);  
  		//printf("scapdu_connect fail, ret = %08Xh\n", ret); 
  		//return false; 
  	}
  } while (ret!=0);
  
	/* Show context */ 
	scapdu_show_ctx(&scapdu_ctx);
  
  //import key 00 14 04 00 20 [Prk]
  printf("ImportKey\n"); 
  memset(apdu, 0x00, sizeof(apdu));
  apdu[0]=0x00;
  apdu[1]=0x14;
  apdu[2]=0x04;
  apdu[3]=0x00;
  apdu[4]=0x20;
  memcpy(apdu+5, vch, 32);
  apdu_len = 37;
  
	apdursp_len = APDURSP_LEN;
	ret = scapdu_send_apdu(&scapdu_ctx, 
		apdu, apdu_len, 
		apdursp, &apdursp_len, 1); 
		
	if (ret != 0) { 
		printf("scapdu_send_apdu fail, ret = %08Xh\n", ret); 
		return false; 
	}  

  //sign hash  00 14 00 00 40 [msg] [k]
  printf("Sign Hash\n");
  memset(apdu, 0x00, sizeof(apdu)); 
  apdu[0]=0x00;
  apdu[1]=0x14;
  apdu[2]=0x00;
  apdu[3]=0x00;
  apdu[4]=0x40;
  memcpy(apdu+5, hash, 32);
  memcpy(apdu+37, hash, 32); //use hash as random bytes
  apdu[37]=0x01;apdu[38]=0x02;
  apdu_len = 69;
  
	apdursp_len = APDURSP_LEN;
	ret = scapdu_send_apdu(&scapdu_ctx, 
		apdu, apdu_len, 
		apdursp, &apdursp_len, 1); 
		
	if (ret != 0) { 
		printf("scapdu_send_apdu fail, ret = %08Xh\n", ret); 
		return false; 
	}

  // Sign
  vchSig.clear();
  nSize = 128;
  vchSig.resize(nSize); // Make sure it is big enough
  nSize = sig_der(apdursp, apdursp+32, (unsigned char*)&vchSig[0]); 
  vchSig.resize(nSize); // Shrink to fit actual size

  fprintf(fp, "sig: ");
  for(unsigned int i=0; i<vchSig.size(); i++)
      fprintf(fp, "%02X", vchSig[i]);
  fprintf(fp, "\n");
  fclose(fp);

	scapdu_release(&scapdu_ctx); 
	return true; 

}



bool ECDSA_Sign(const unsigned char* vch, unsigned char* hash, std::vector<unsigned char>& vchSig) {
  BIGNUM bn;
  EC_KEY *pkey;
  unsigned int nSize;
  FILE *fp;
  
  time_t rawtime;
  struct tm * timeinfo;
  
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  
  fp=fopen("libikv.log", "at");
  
  fprintf (fp, "SW_Sign %s", asctime (timeinfo) );
  
  fprintf(fp, "key: ");
  for(unsigned int i=0; i<32; i++)
      fprintf(fp, "%02X", vch[i]);
  fprintf(fp, "\n");
  
  fprintf(fp, "hash: ");
  for(unsigned int i=0; i<32; i++)
      fprintf(fp, "%02X", hash[i]);
  fprintf(fp, "\n");
  
  // Set key
  BN_init(&bn);
  assert((pkey = EC_KEY_new_by_curve_name(NID_secp256k1)) != NULL);
  assert(BN_bin2bn(vch, 32, &bn));
  assert(EC_KEY_set_private_key(pkey, &bn));
  BN_clear_free(&bn);
  
  // Sign
  vchSig.clear();
  nSize = ECDSA_size(pkey);
  vchSig.resize(nSize); // Make sure it is big enough
  if (ECDSA_sign(0, hash, 32, (unsigned char*)&vchSig[0], &nSize, pkey) == 0)
      return false;
  vchSig.resize(nSize); // Shrink to fit actual size
  EC_KEY_free(pkey);
  
  fprintf(fp, "sig: ");
  for(unsigned int i=0; i<vchSig.size(); i++)
      fprintf(fp, "%02X", vchSig[i]);
  fprintf(fp, "\n");
  fclose(fp);
  
  return true;
}