#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <time.h> 
#include "util.h" 

/* Hex string to binary array */ 
int hs2ba(u1 *ba, u4 *size, const char *hs, u4 hslen) 
{
	u4 i, balen; 
	u1 *bap; 
	const char *hsp; 
	int tmp; 
	
	/* Check buffer size */ 
	balen = hslen / 2; 
	if (*size < balen) {  /* size as buffer size */ 
		return -1; 
	}
	
	bap = ba; 
	hsp = hs; 
	for (i = 0; i < balen; i++) { 
		if (sscanf(hsp, "%02X", &tmp) != 1) {
			return -2; 
		}
		
		*bap++ = (u1) tmp; 
		hsp += 2; 
	}
	
	*size = balen; 
	
	return 0; 
}


#define LINE_BYTES    16 
void print_ba(u1 *ba,  u2  len, u2  pre_blank) 
{ 
	u2  i; 
	char  blanks[LINE_BYTES + 1]; 
	
	memset(blanks, ' ', sizeof(blanks)); 
	
	if (pre_blank >= LINE_BYTES) { 
		blanks[LINE_BYTES] = '\0'; 
	} else { 
		blanks[pre_blank] = '\0'; 
	}
	
	for (i = 0; i < len; i++) { 
		printf("%02X ", ba[i]); 
		
		if ((i % LINE_BYTES) == (LINE_BYTES - 1)) { 
			putchar('\n'); 
			
			/* Pre-blanks for next line */ 
			if (i != (len - 1)) { 
				printf("%s", blanks); 
			}
		}
	}
	
	if ((i % LINE_BYTES) != 0) { 
		putchar('\n'); 
	}
}


/* Show binary array and description */ 
void show_ba(const char *desc, u1 *ba,  u2  len) 
{ 
	printf("%s\n", desc); 
	print_ba(ba,  len, 0); 
	putchar('\n'); 
}

/* Print word array */ 
void print_wa(u4 *wa,  u2  nw) 
{ 
	u2  i; 
	
	for (i = 0; i < nw; i++) { 
		printf("%08X ", wa[i]); 
		
		if ((i % 8) == 7) { 
			putchar('\n'); 
		}
	}
	
	if ((i % 8) != 0) { 
		putchar('\n'); 
	}
}


/* Show word array and description */ 
void show_wa(const char *desc, u4 *wa,  u2  nw) 
{ 
	printf("%s\n", desc); 
	print_wa(wa,  nw); 
	putchar('\n'); 
}


/* Initiaize random module */ 
void rand_init(void) 
{ 
	u2  i; 
	
	srand ((unsigned long) time(NULL)); 
	
	for (i = 0; i < 1000; i++) { 
		rand(); 
	}
}


/*** 
 * Get random bytes 
 * [NOTE] nonce can be NULL 
 */ 
int rand_get_bytes (
	IN     u2  nbytes, 
	OUT    u1 *buf, 
	IN     u2  nonce_len, 
	IN     u1 *nonce) 
{ 
	u2  i; 
	
	for (i = 0; i < nbytes; i++) { 
		buf[i] = (rand() & 0x1fe) >> 1; 
	}
	
	/* Nonce not used here */ 

	return 0; 
}

/* Delete spaces in a string */ 
void str_delspc(char *str)
{
	u4 len; 
	u4 i, j, k; 
	
	len = (u4) strlen(str); 
	
	i = 0; 
	for (k = 0; k < len; k++) { 
		if ((str[i] == ' ') || (str[i] == '\n') || (str[i] == '\t')) { 
			/* Shift left 1 char */ 
			for (j = i; j < (len - 1); j++) { 
				str[j] = str[j + 1]; 
			}
			len--; 
			k--; 
		} else { 
			i++; 
		}
	}
	
	str[len] = '\0'; 
}

