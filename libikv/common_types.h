/*** 
 * common_types.h: Common type definitions for all projects 
 */ 

#ifndef __COMMON_TYPES_H__ 
#define __COMMON_TYPES_H__ 

#include <stdint.h> 

typedef unsigned char  u1; 
typedef unsigned short u2; 
typedef unsigned long  u4; 
typedef unsigned int   ITEM; 
typedef unsigned int   FLAG; 
typedef unsigned long   USIZ; 

#define IN 
#define OUT 

#endif 
