#include <stdio.h> 
#include <windows.h> 
#include "scapdu.h" 
#include "scapdu_err.h" 

/***************************************************************/ 
/***************** MACRO and TYPE definitions ******************/ 
/***************************************************************/ 

// #define SCAPDU_VERBOSE_MSG 

#define SCAPDU_STATE_SCCTX        (1 << 0)   /* Context established */ 
#define SCAPDU_STATE_SCCONNECT    (1 << 1)   /* Smartcard connected */ 

#define READER_LIST_MAX            512       /* Maximal reader list length (in bytes) */ 

/* SCAPDU library verson */ 
#define SCAPDU_VER    "\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00" 

/***************************************************************/ 
/********************** Global variables ***********************/ 
/***************************************************************/ 

/***************************************************************/ 
/********************** Local Utilities ************************/ 
/***************************************************************/ 

/* APDU transmission function for T = 0 protocol */ 
int scapdu_send_apdu_t0( 
	IN     scapdu_ctx_t  *ctx, 
	IN     BYTE          *apdu, 
	IN     USIZ           apdu_len, 
	IN     BYTE          *apdursp, 
	IN OUT USIZ          *apdursp_len, 
	IN     FLAG           auto_getrsp)
{ 
	int   ret = 0; 
	LONG  sc_ret; 
	BYTE  rsp[SCAPDU_RDATA_MAX + 2]; 
	USIZ  rsp_len; 
	
	/* Transmit APDU */ 
	rsp_len = sizeof(rsp); 
	sc_ret = SCardTransmit(ctx->sc_handle, 
		SCARD_PCI_T0,    /* Protocol control information */ 
		apdu, apdu_len,  /* APDU command */ 
		NULL, 
		rsp, &rsp_len);  /* APDU response */ 

	if (sc_ret != SCARD_S_SUCCESS) { 
#ifdef SCAPDU_VERBOSE_MSG 
		printf("[scapdu_send_apdu] Smartcard transmission fail (ret = %08xh)\n", sc_ret); 
#endif 	
		ret = SCAPDU_ERR_TRANSMIT; 
		goto __func_end; 
	}
	
	if (auto_getrsp) { 
		/* Auto get response if nessary */ 
		BYTE  apdu_getrsp[APDU_HEADER_LEN] = {0x00, 0xC0, 0x00, 0x00, 0x00}; 
				
		if ((rsp_len == 2) && (rsp[0] == 0x61)) { 
			apdu_getrsp[APDU_HEADER_LEN - 1] = rsp[1]; 
			
			/* Get response */ 
			rsp_len = sizeof(rsp); 
			sc_ret = SCardTransmit(ctx->sc_handle, 
				SCARD_PCI_T0,    /* Protocol control information */ 
				apdu_getrsp, APDU_HEADER_LEN,  /* APDU command */ 
				NULL, 
				rsp, &rsp_len);  /* APDU response */ 

			if (sc_ret != SCARD_S_SUCCESS) { 
#ifdef SCAPDU_VERBOSE_MSG 
				printf("[scapdu_send_apdu] Smartcard transmission fail (ret = %08xh)\n", sc_ret); 
#endif 	
				ret = SCAPDU_ERR_TRANSMIT; 
				goto __func_end; 
			}			
		}
	}
	
	if ((*apdursp_len) < rsp_len) { 
		ret = SCAPDU_ERR_BUFSIZ; 
		goto __func_end; 
	}
	
	*apdursp_len = rsp_len; 
	memcpy(apdursp, rsp, rsp_len); 

__func_end: 
	return ret; 
}


/***************************************************************/ 
/******************** Exported Functions ***********************/ 
/***************************************************************/ 

/*** 
 * [Function   ] scapdu_connect 
 * [Description] Connect designated smartcard or find the first smartcard can be connected 
 * [Parameters ] 
 *       (out) ctx    : SCAPDU context 
 *       (in ) reader : Reader string 
 * [Return     ] 0: success (Error code in scapdu_err.h) 
 * [Note       ] 
 *    -- This function will establish context, and connect to designated smartcard 
 *    -- If parameter "reader" is NULL, this function will find the first smartcard 
 *       which is successfully connected 
 */ 
int scapdu_connect( 
	OUT    scapdu_ctx_t   *ctx, 
	IN     LPTSTR         *reader) 
{ 
	SCARDCONTEXT  sc_ctx; 
	SCARDHANDLE   sc_handle; 
	ITEM    sc_state = 0; 
	LONG    sc_ret; 
	int     ret = 0; 
	DWORD   cchReaders; 
	TCHAR   mszReaders[READER_LIST_MAX]; 
	LPTSTR  pReader; 
	DWORD   actvProtocol; 
	FLAG    sc_connect = 0;  /* Smartcard connected flag */ 
	
	/*** 
	 * 1. Establish smartcard context 
	 * 2. If reader is NULL, list all readers 
	 * 3. Connect designated reader or listed readers in previous step 
	 */ 
	
	if (ctx == NULL) { 
		return SCAPDU_ERR_PARAM; 
	}
	 
	/* 1. Establish smartcard context */ 
	sc_ret = SCardEstablishContext(SCARD_SCOPE_USER, NULL, NULL, &sc_ctx); 
	if (sc_ret != SCARD_S_SUCCESS) { 
#ifdef SCAPDU_VERBOSE_MSG 
		printf("[scapdu_connect] Establish context fail (ret = %08xh\n)", sc_ret); 
#endif 	
		ret = SCAPDU_ERR_SCCTX; 
		goto __func_end; 
	}
	
	/* Establish context success */ 
	sc_state |= SCAPDU_STATE_SCCTX; 
	ctx->sc_ctx = sc_ctx; 
	
	/* 2. If reader is NULL, list all readers */ 
	if (reader == NULL) { 
		cchReaders = sizeof(mszReaders); 
		sc_ret = SCardListReaders(sc_ctx, NULL, mszReaders, &cchReaders); 
		if (sc_ret != SCARD_S_SUCCESS) { 
#ifdef SCAPDU_VERBOSE_MSG 
			printf("[scapdu_connect] List reader fail (ret = %08xh)\n", sc_ret); 
#endif 	
			ret = SCAPDU_ERR_READER; 
			goto __func_end; 
		}		
	}
	
	/* 3. Connect designated reader or listed readers in previous step */ 
	pReader = mszReaders; 
	while ((*pReader) != '\0') { 		
		sc_ret = SCardConnect( 
			sc_ctx,                 /* Smartcard context */ 
			pReader,                /* Reader name */ 
			SCARD_SHARE_EXCLUSIVE,  /* SCARD_SHARE_SHARED */ 
			SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1,  /* Allowed protocol */ 
			&sc_handle,             /* Smartcard handle */ 
			&actvProtocol);         /* Active protocol */ 
			
		if (sc_ret == 0) { 
			/* Smartcard connected */ 
			sc_connect = 1; 
			break; 
		}
		
		
		pReader = pReader + strlen(pReader) + 1; 
	}
	
	if (!sc_connect) { 
#ifdef SCAPDU_VERBOSE_MSG 
		printf("[scapdu_connect] No smartcard connected\n", sc_ret); 
#endif 	
		ret = SCAPDU_ERR_SCCONNECT; 
		goto __func_end; 
	}
	
	/* Smartcard connected */ 
	sc_state |= SCAPDU_STATE_SCCONNECT; 
	ctx->sc_handle   = sc_handle; 
	ctx->sc_protocol = actvProtocol; 
	
__func_end: 
	ctx->sc_state = sc_state; 
	
	return ret; 
}
	
	
/*** 
 * [Function   ] scapdu_release 
 * [Description] Release SCAPDU context 
 * [Parameters ] 
 *       (in ) ctx    : SCAPDU context 
 * [Return     ] 0: success (Error code in scapdu_err.h) 
 * [Note       ] 
 *    -- This function will release contexts and handles in SCAPDU context 
 *    -- Released SCAPDU context cannot be used again 
 */ 
int scapdu_release( 
	IN OUT scapdu_ctx_t   *ctx) 
{ 	
	if (ctx == NULL) { 
		return SCAPDU_ERR_PARAM; 
	}
		
	/* Disconnect smartcard */ 
	if ((ctx->sc_state) & SCAPDU_STATE_SCCONNECT) { 
		SCardDisconnect(ctx->sc_handle, SCARD_LEAVE_CARD); 
		(ctx->sc_state)  &= (~SCAPDU_STATE_SCCONNECT); 
	}

	/* Release smartcard context */ 
	if ((ctx->sc_state) & SCAPDU_STATE_SCCTX) { 
		SCardReleaseContext(ctx->sc_ctx); 
		(ctx->sc_state)  &= (~SCAPDU_STATE_SCCTX); 
	}

	return 0; 
}
	
	
/*** 
 * [Function   ] scapdu_send_apdu 
 * [Description] Send APDU command to connected smartcard 
 * [Parameters ] 
 *       (in ) ctx         : SCAPDU context 
 *       (in ) apdu        : APDU command to be sent 
 *       (in ) apdu_len    : APDU command length (in bytes) 
 *       (out) apdursp     : Response data and status word 
 *       (in ) apdursp_len : Response data buffer length (in bytes) 
 *       (out) apdursp_len : Response data length (in bytes) 
 *       (in ) auto_getrsp : Auto get APDU response (in T = 0 protocol) 
 * [Return     ] 0: success (Error code in scapdu_err.h) 
 * [Note       ] 
 *    -- User needs to fill rdata buffer pointer and buffer size in apdursp parameter 
 */ 
int scapdu_send_apdu( 
	IN     scapdu_ctx_t  *ctx, 
	IN     BYTE          *apdu, 
	IN     USIZ           apdu_len, 
	IN     BYTE          *apdursp, 
	IN OUT USIZ          *apdursp_len, 
	IN     FLAG           auto_getrsp)
{ 
	int   ret = 0; 
	
	if ((ctx         == NULL) || 
	    (apdursp     == NULL) || 
	    (apdursp_len == NULL)) { 
		return SCAPDU_ERR_PARAM; 
	}
	
	/* Check smartcard state */ 
	if ((!((ctx->sc_state) & SCAPDU_STATE_SCCTX)) || 
	    (!((ctx->sc_state) & SCAPDU_STATE_SCCONNECT))) { 
		ret = SCAPDU_ERR_STATE; 
		goto __func_end; 
	}
		
	if ((ctx->sc_protocol) != SCARD_PROTOCOL_T0) { 
		/* Currently only T = 0 is supported */ 
		ret = SCAPDU_ERR_PROTOCOL; 
		goto __func_end; 
	}
	
	/* Check APDU length */ 
	if (apdu_len < APDU_HEADER_LEN) { 
		return SCAPDU_ERR_APDULEN; 
	}
	
	/* APDU transmission (Protocol T = 0) */ 
	ret = scapdu_send_apdu_t0(ctx, apdu, apdu_len, apdursp, apdursp_len, auto_getrsp); 

__func_end: 
	return ret; 
}
	
	
	
/*** 
 * [Function   ] scapdu_show_ctx 
 * [Description] Show SCAPDU context information 
 * [Parameters ] 
 *       (in ) ctx     : SCAPDU context 
 * [Return     ] 0: success (Error code in scapdu_err.h) 
 * [Note       ] 
 *    -- User needs to fill rdata buffer pointer and buffer size in apdursp parameter 
 */ 
int scapdu_show_ctx( 
	IN     scapdu_ctx_t      *ctx) 
{ 
	if (ctx == NULL) { 
		return SCAPDU_ERR_PARAM; 
	}
	
	printf("[sc_ctx     ] %08xh\n", ctx->sc_ctx); 
	printf("[sc_handle  ] %08xh\n", ctx->sc_handle); 
	
	/* Protocol */ 
	if ((ctx->sc_protocol) == SCARD_PROTOCOL_T0) { 
		printf("[sc_protocol] T = 0\n"); 
	} else if ((ctx->sc_protocol) == SCARD_PROTOCOL_T1) { 
		printf("[sc_protocol] T = 1\n"); 
	} else { 
		printf("[sc_protocol] Unknown\n"); 
	}
	
	/* State */ 
	if ((ctx->sc_state) & SCAPDU_STATE_SCCONNECT) { 
		printf("[sc_state   ] Smartcard connected\n"); 
	} else if ((ctx->sc_state) & SCAPDU_STATE_SCCTX) { 
		printf("[sc_state   ] Context established\n"); 
	} else { 
		printf("[sc_state   ] Context NOT established\n"); 
	}
	
	
	return 0; 
}

	
/*** 
 * [Function   ] scapdu_version 
 * [Description] Get SCAPDU library version 
 * [Parameters ] 
 *       (out) ver : SCAPDU library version (16 bytes) 
 * [Return     ] 0: success (Error code in scapdu_err.h) 
 * [Note       ] 
 *    -- 
 */ 
int scapdu_version( 
	OUT    BYTE  *ver) 
{ 
	if (ver == NULL) { 
		return SCAPDU_ERR_PARAM; 
	}
	
	memcpy(ver, SCAPDU_VER, SCAPDU_VERLEN); 
	
	return 0; 
}

