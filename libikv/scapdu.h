/***
 * [ Project     ] scapdu 
 * [ Module      ] scapdu 
 * [ Description ] Smartcard APDU transmission interface 
 * [ Note        ] 
 */ 

#ifndef __SCAPDU_H__ 
#define __SCAPDU_H__ 

#include "common.h" 
#include <winscard.h> 

/***************************************************************/ 
/***************** MACRO and TYPE definitions ******************/ 
/***************************************************************/ 

#define SCAPDU_CDATA_MAX    256    /* Maximal carried data length (in bytes) */ 
#define SCAPDU_RDATA_MAX    256    /* Maximal response data length (in bytes) */ 

#define SCAPDU_VERLEN       16     /* SCAPDU library version length (in bytes) */ 

/* SCAPDU context structure */ 
typedef struct scapdu_ctx_s { 
	SCARDCONTEXT  sc_ctx;        /* Smartcard context in winscard lib */ 
	SCARDHANDLE   sc_handle;     /* Smartcard handle  in winscard lib */ 
	DWORD         sc_protocol;   /* Smartcard protocol (T = 0 or T = 1) */ 
	ITEM          sc_state;      /* Connecting state */ 
} scapdu_ctx_t; 


/* APDU header structure */ 
#define APDU_HEADER_LEN    5         /* APDU header length (in bytes) */ 
#define APDU_CLA           0         /* APDU CLASS byte */ 
#define APDU_INS           1         /* APDU INSTRUCTION byte */ 
#define APDU_P1            2         /* APDU PARAMETER 1 */ 
#define APDU_P2            3         /* APDU PARAMETER 2 */ 
#define APDU_P3            4         /* APDU PARAMETER 3 */ 
#define APDU_LC            4         /* APDU LC byte (carried data length in bytes) */ 


#ifdef __cpludplus 
extern "C" { 
#endif 

/***************************************************************/ 
/******************** Exported Functions ***********************/ 
/***************************************************************/ 

/*** 
 * [Function   ] scapdu_connect 
 * [Description] Connect designated smartcard or find the first smartcard can be connected 
 * [Parameters ] 
 *       (out) ctx    : SCAPDU context 
 *       (in ) reader : Reader string 
 * [Return     ] 0: success (Error code in scapdu_err.h) 
 * [Note       ] 
 *    -- This function will establish context, and connect to designated smartcard 
 *    -- If parameter "reader" is NULL, this function will find the first smartcard 
 *       which is successfully connected 
 */ 
int scapdu_connect( 
	OUT    scapdu_ctx_t   *ctx, 
	IN     LPTSTR         *reader); 
	
	
/*** 
 * [Function   ] scapdu_release 
 * [Description] Release SCAPDU context 
 * [Parameters ] 
 *       (in ) ctx    : SCAPDU context 
 * [Return     ] 0: success (Error code in scapdu_err.h) 
 * [Note       ] 
 *    -- This function will release contexts and handles in SCAPDU context 
 *    -- Released SCAPDU context cannot be used again 
 */ 
int scapdu_release( 
	IN OUT scapdu_ctx_t   *ctx); 
	
	
/*** 
 * [Function   ] scapdu_list_readers 
 * [Description] List all smartcard readers 
 * [Parameters ] 
 *       (out) reader_strs     : Reader strings 
 *       (in ) reader_strs_len : Buffer length (in bytes) 
 *       (out) reader_strs_len : Reader strings length (in bytes) 
 * [Return     ] 0: success (Error code in scapdu_err.h) 
 * [Note       ] 
 *    -- This function will establish context, and connect to designated smartcard 
 *    -- If parameter "reader" is NULL, this function will find the first smartcard 
 *       which is successfully connected 
 */ 
int scapdu_list_readers( 
	OUT     LPTSTR  *reader_strs, 
	IN OUT  DWORD   *reader_strs_len); 

	
/*** 
 * [Function   ] scapdu_send_apdu 
 * [Description] Send APDU command to connected smartcard 
 * [Parameters ] 
 *       (in ) ctx         : SCAPDU context 
 *       (in ) apdu        : APDU command to be sent 
 *       (in ) apdu_len    : APDU command length (in bytes) 
 *       (out) apdursp     : Response data and status word 
 *       (in ) apdursp_len : Response data buffer length (in bytes) 
 *       (out) apdursp_len : Response data length (in bytes) 
 *       (in ) auto_getrsp : Auto get APDU response (in T = 0 protocol) 
 * [Return     ] 0: success (Error code in scapdu_err.h) 
 * [Note       ] 
 *    -- User needs to fill rdata buffer pointer and buffer size in apdursp parameter 
 */ 
int scapdu_send_apdu( 
	IN     scapdu_ctx_t  *ctx, 
	IN     BYTE          *apdu, 
	IN     USIZ           apdu_len, 
	IN     BYTE          *apdursp, 
	IN OUT USIZ          *apdursp_len, 
	IN     FLAG           auto_getrsp); 
	
	
/*** 
 * [Function   ] scapdu_show_ctx 
 * [Description] Show SCAPDU context information 
 * [Parameters ] 
 *       (in ) ctx     : SCAPDU context 
 * [Return     ] 0: success (Error code in scapdu_err.h) 
 * [Note       ] 
 *    -- User needs to fill rdata buffer pointer and buffer size in apdursp parameter 
 */ 
int scapdu_show_ctx( 
	IN     scapdu_ctx_t      *ctx); 
	
	
/*** 
 * [Function   ] scapdu_version 
 * [Description] Get SCAPDU library version 
 * [Parameters ] 
 *       (out) ver : SCAPDU library version (16 bytes) 
 * [Return     ] 0: success (Error code in scapdu_err.h) 
 * [Note       ] 
 *    -- 
 */ 
int scapdu_version( 
	OUT    BYTE  *ver); 
	
	
#ifdef __cpludplus 
}
#endif 

#endif 
