/*** 
 * input: 
 *   -- sig_r: 32 bytes 
 *   -- sig_s: 32 bytes 
 *
 * output: 
 *   -- data: DER of (sig_r || sig_s) -> "index" bytes 
 */ 

static void sig_der(void) 
{
	/* Get ECDSA Signature */
	u1 tlen = 0x44;
	u1 rlen = 0x20;
	u1 slen = 0x20;
	u4 index = 0; 
	
	if (sig_r[0] & 0x80) {
		tlen++;
		rlen++;
	} 
	
	if (sig_s[0] & 0x80) {
		tlen++;
		slen++;
	}

	data[index++] = 0x30;
	data[index++] = tlen;
	data[index++] = 0x02;
	data[index++] = rlen;
	if (rlen == 0x21) {
		data[index++] = 0x00;
	}
	memcpy(data+index, sig_r, 32);
	index += 32;
	data[index++] = 0x02;
	data[index++] = slen;
	if (slen == 0x21) {
		data[index++] = 0x00;
	}
	memcpy(data+index, sig_s, 32);
	index += 32;
	*rsp_size = index;

	return 0;
}
