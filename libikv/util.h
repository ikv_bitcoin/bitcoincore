#ifndef __UTIL_H__ 
#define __UTIL_H__ 

#include "types.h" 

#ifdef __cplusplus 
extern "C" { 
#endif 

/* Hex string to binary array */ 
int hs2ba(u1 *ba, u4 *size, const char *hs, u4 hslen); 

void print_ba(u1 *ba,  u2  len, u2  pre_blank); 

/* Show binary array and description */ 
void show_ba(const char *desc, u1 *ba,  u2  len); 

/* Print word array */ 
void print_wa(u4 *wa,  u2  nw); 

/* Show word array and description */ 
void show_wa(const char *desc, u4 *wa,  u2  nw); 

/* Initiaize random module */ 
void rand_init(void); 


/*** 
 * Get random bytes 
 * [NOTE] nonce can be NULL 
 */ 
int rand_get_bytes (
	IN     u2  nbytes, 
	OUT    u1 *buf, 
	IN     u2  nonce_len, 
	IN     u1 *nonce); 
	
	
/* Delete spaces in a string */ 
void str_delspc(char *str); 
	
#ifdef __cplusplus 
}
#endif 

#endif 
