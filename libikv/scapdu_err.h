/*** 
 * scapdu_err.h: Error code list for SCAPDU library 
 */ 

#ifndef __SCAPDU_ERR_H__ 
#define __SCAPDU_ERR_H__ 

#define SCAPDU_ERR_BASE         0 
#define SCAPDU_ERR_PARAM       (SCAPDU_ERR_BASE + 0x01)  /* Wrong parameters */ 
#define SCAPDU_ERR_SCCTX       (SCAPDU_ERR_BASE + 0x02)  /* Fail to establish smartcard context */ 
#define SCAPDU_ERR_READER      (SCAPDU_ERR_BASE + 0x03)  /* Fail to list smartcard readers */ 
#define SCAPDU_ERR_SCCONNECT   (SCAPDU_ERR_BASE + 0x04)  /* Fail to connect smartcard */ 
#define SCAPDU_ERR_STATE       (SCAPDU_ERR_BASE + 0x05)  /* Wrong smartcard state */ 
#define SCAPDU_ERR_PROTOCOL    (SCAPDU_ERR_BASE + 0x06)  /* Protocol not supported */ 
#define SCAPDU_ERR_APDULEN     (SCAPDU_ERR_BASE + 0x07)  /* Wrong APDU length */ 
#define SCAPDU_ERR_TRANSMIT    (SCAPDU_ERR_BASE + 0x08)  /* Fail to transmit APDU */ 
#define SCAPDU_ERR_BUFSIZ      (SCAPDU_ERR_BASE + 0x09)  /* Buffer size not enough */ 

#endif 
