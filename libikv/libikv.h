#ifndef LIBIKV_H_

#define LIBIKV_H_


bool SC_Sign(const unsigned char* vch, unsigned char* hash, std::vector<unsigned char>& vchSig);


bool ECDSA_Sign(const unsigned char* vch, unsigned char* hash, std::vector<unsigned char>& vchSig);


#endif
